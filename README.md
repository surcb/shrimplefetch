# shrimplefetch

*"It really is this shrimple"*

How 2 use:

1: Run the following command to install the program to your /usr/bin and to add the `shrimp` alias to your `~/.bash_aliases`:
		`./insaller.sh`

2: Run `shrimp` from anywhere

3: ???

4: Profit

You can test the program without installing by running `./shrimplefetch.sh`
