#!/bin/sh

GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Get vendor name by ID
get_vendor_name() {
    case $1 in
        0x1002) printf "AMD";;
        0x1010) printf "ImgTec";;
        0x10de) printf "NVIDIA";;
        0x13b5) printf "ARM";;
        0x5143) printf "Qualcomm";;
        0x8086) printf "INTEL";;
        *) printf "Unknown ($1)";;
    esac
}

# Get OS pretty name
os_name=$(awk -F= '/^PRETTY_NAME/ {print $2}' /etc/os-release | tr -d '"' 2>/dev/null || uname -o)

# Get kernel version
kernel_version=$(uname -r)

# Display OS and kernel info
printf "${GREEN}OS${NC}\t%s\n" "$os_name"
printf "${GREEN}KER${NC}\t%s\n" "$kernel_version"

# Display uptime if available
if [ -e /proc/uptime ]; then
    up=$(cut -d. -f1 /proc/uptime)
    days=$((up / 86400))
    hours=$((up / 3600 % 24))
    mins=$((up / 60 % 60))
    uptime=""
    [ $days -gt 0 ] && uptime="${days} day$(if [ $days -gt 1 ]; then echo "s"; else echo ""; fi), "
    [ $hours -gt 0 ] && uptime="${uptime}${hours} hour$(if [ $hours -gt 1 ]; then echo "s"; else echo ""; fi), "
    uptime="${uptime}${mins} min$(if [ $mins -gt 1 ]; then echo "s"; else echo ""; fi)"
    printf "${GREEN}UPT${NC}\t%s\n" "$uptime"
fi

# Get and display SHELL info
if [ -n "$SHELL" ]; then
    shell_basename=$(basename "$SHELL")
    printf "${GREEN}SHL${NC}\t%s\n" "$shell_basename"
fi

# Get and display WM info
if [ -n "$DESKTOP" ]; then
    printf "${GREEN}DSK${NC}\t%s\n" "$DESKTOP"
fi

# Get and display Terminal
if [ -e /proc/$PPID/exe ]; then
    pid=$$
    while [ "$(readlink /proc/$pid/exe)" = "$(readlink /proc/$PPID/exe)" ]; do
        pid=$(awk '{print $4}' /proc/$pid/stat)
    done

    terminal=$(awk '{print $1}' /proc/$pid/comm)
    printf "${GREEN}TER${NC}\t%s\n" "$terminal"
fi

# Get and display CPU info
if [ -f /proc/cpuinfo ]; then
    cpu=$(awk -F ': ' '/^model name/ {print $2; exit}' /proc/cpuinfo)
    threads=$(awk '/^processor/ {count++} END {print count}' /proc/cpuinfo)
    printf "${GREEN}CPU${NC}\t%s (%s)\n" "$cpu" "$threads"
fi

# Get and display RAM info
if [ -f /proc/meminfo ]; then
    ram_info=$(free -m | awk '/^Mem:/ {used=$3; total=$2; print used, total}')
    used_ram=$(echo $ram_info | awk '{print $1}')
    total_ram=$(echo $ram_info | awk '{print $2}')
    printf "${GREEN}RAM${NC}\t%sMiB / %sMiB\n" "$used_ram" "$total_ram"
fi

# Get and display GPU info
#if [ -d /sys/class/drm ]; then
#    for gpu in /sys/class/drm/card*; do
#        if [ -d "$gpu/device" ]; then
#            vendor=$(cat "$gpu/device/vendor")
#            device=$(cat "$gpu/device/device")
#            vendor_name=$(get_vendor_name $vendor)
#            printf "${GREEN}GPU${NC}\t%s - %s\n" "$vendor_name" "$device"
#        fi
#    done
#fi


# Get and print product version
systype=$(cat /sys/devices/virtual/dmi/id/product_version)
printf "${GREEN}SYS${NC}\t%s\n" "$systype"
